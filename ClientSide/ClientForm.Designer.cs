﻿namespace ClientSide
{
    partial class ClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SubmitMessage = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.Message_textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.DisplayMessage = new System.Windows.Forms.Button();
            this.IP_Address_textBox = new System.Windows.Forms.TextBox();
            this.SendPort_textBox = new System.Windows.Forms.TextBox();
            this.Connect = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // SubmitMessage
            // 
            this.SubmitMessage.Enabled = false;
            this.SubmitMessage.Location = new System.Drawing.Point(175, 337);
            this.SubmitMessage.Name = "SubmitMessage";
            this.SubmitMessage.Size = new System.Drawing.Size(85, 49);
            this.SubmitMessage.TabIndex = 0;
            this.SubmitMessage.Text = "Submit Message";
            this.SubmitMessage.UseVisualStyleBackColor = true;
            this.SubmitMessage.Click += new System.EventHandler(this.SubmitMessage_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 303);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "Message to send";
            // 
            // Message_textBox
            // 
            this.Message_textBox.Location = new System.Drawing.Point(175, 303);
            this.Message_textBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Message_textBox.Name = "Message_textBox";
            this.Message_textBox.Size = new System.Drawing.Size(246, 26);
            this.Message_textBox.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(108, 243);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 20);
            this.label1.TabIndex = 10;
            this.label1.Text = "Topic";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Items.AddRange(new object[] {
            "News & Announcements",
            "General",
            "Chat Room",
            "Urgent"});
            this.listBox1.Location = new System.Drawing.Point(175, 223);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(246, 64);
            this.listBox1.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(139, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 22);
            this.label2.TabIndex = 12;
            this.label2.Text = "Bulletin-System";
            // 
            // DisplayMessage
            // 
            this.DisplayMessage.Location = new System.Drawing.Point(336, 337);
            this.DisplayMessage.Name = "DisplayMessage";
            this.DisplayMessage.Size = new System.Drawing.Size(85, 49);
            this.DisplayMessage.TabIndex = 13;
            this.DisplayMessage.Text = "Display Message";
            this.DisplayMessage.UseVisualStyleBackColor = true;
            this.DisplayMessage.Click += new System.EventHandler(this.DisplayMessage_Click);
            // 
            // IP_Address_textBox
            // 
            this.IP_Address_textBox.BackColor = System.Drawing.SystemColors.Menu;
            this.IP_Address_textBox.Location = new System.Drawing.Point(12, 360);
            this.IP_Address_textBox.Name = "IP_Address_textBox";
            this.IP_Address_textBox.Size = new System.Drawing.Size(139, 26);
            this.IP_Address_textBox.TabIndex = 15;
            this.IP_Address_textBox.Text = "127.0.0.1";
            this.IP_Address_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // SendPort_textBox
            // 
            this.SendPort_textBox.BackColor = System.Drawing.SystemColors.MenuBar;
            this.SendPort_textBox.Location = new System.Drawing.Point(12, 326);
            this.SendPort_textBox.Name = "SendPort_textBox";
            this.SendPort_textBox.Size = new System.Drawing.Size(100, 26);
            this.SendPort_textBox.TabIndex = 16;
            // 
            // Connect
            // 
            this.Connect.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Connect.Location = new System.Drawing.Point(12, 239);
            this.Connect.Name = "Connect";
            this.Connect.Size = new System.Drawing.Size(89, 28);
            this.Connect.TabIndex = 17;
            this.Connect.Text = "Connect";
            this.Connect.UseVisualStyleBackColor = false;
            this.Connect.Click += new System.EventHandler(this.Connect_Click);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(28, 61);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(393, 147);
            this.textBox1.TabIndex = 18;
            // 
            // ClientForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(442, 398);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Connect);
            this.Controls.Add(this.SendPort_textBox);
            this.Controls.Add(this.IP_Address_textBox);
            this.Controls.Add(this.DisplayMessage);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Message_textBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.SubmitMessage);
            this.Name = "ClientForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.ClientSide_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SubmitMessage;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Message_textBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button DisplayMessage;
        private System.Windows.Forms.TextBox IP_Address_textBox;
        private System.Windows.Forms.TextBox SendPort_textBox;
        private System.Windows.Forms.Button Connect;
        private System.Windows.Forms.TextBox textBox1;
    }
}

