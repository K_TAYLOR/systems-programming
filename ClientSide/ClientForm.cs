﻿     using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net; //provides simple programming aspects for many protocols
using System.Net.Sockets; //provides the implementation of windows sockets

namespace ClientSide
{
    public partial class ClientForm : Form
    {
        private Socket _clientSocket; //The Client Side Socket

        public ClientForm()
        {
            InitializeComponent();
        }

        protected override void OnClosing (CancelEventArgs e)
        {
            //raises the closing event
         base.OnClosing(e);
            if (_clientSocket != null && _clientSocket.Connected)
            {
                _clientSocket.Close();
            }
        }

        //Method to use with the connect button that will establish a connection to the server
        private void Connect_Click(object sender, EventArgs e)
        {
            try
            {
                //creates a socket that can be used to communicate on a TCP/IP-based network
                _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                //Connect to a local host
                _clientSocket.BeginConnect(new IPEndPoint(IPAddress.Parse(IP_Address_textBox.Text), 8000), new AsyncCallback(ConnectCallback), null);
            }
            catch (Exception ex)
            {
                //error message will display if failed to establish a connection with the socket
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ConnectCallback(IAsyncResult AR)
        {
            try
            {
                //ends a pending asynchronous connection request
                _clientSocket.EndConnect(AR);
                //statement becomes true when a connection has been made
                UpdateControlStates(true);
                SubmitMessage.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UpdateControlStates(bool toggle)
        {
            //a delegate that can execute any method in managed code that is declared void and takes no parameters
            MethodInvoker invoker = new MethodInvoker(delegate
            {
                //since the submit butten is set to false, this will become true when it detects an active connection is present to allow the user to send messages
                SubmitMessage.Enabled = toggle;
                SubmitMessage.Enabled = !toggle;
               

            });
        }

        private void SubmitMessage_Click(object sender, EventArgs e)
        {
            try
            {
                //gets or sets a value that specifies the size of the receive buffer of the Socket. encodes into ASCII
                byte[] buffer = Encoding.ASCII.GetBytes(Message_textBox.Text);
                //begin to asynchronously send data from a connected socket
                _clientSocket.BeginSend(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(SendCallback), null);
            }
            catch (SocketException) { } //Server closed 
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SendCallback(IAsyncResult AR)
        {
            try
            {
                //ends a pending asynchronous send 
                _clientSocket.EndSend(AR);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void DisplayMessage_Click(object sender, EventArgs e)
        {
            
        }

        private void ClientSide_Load(object sender, EventArgs e)
        {

        }
    }
   
    }



