﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets; //provides the implementation of windows sockets
using System.Net; //provides simple programming aspects for many protocols
using System.Diagnostics; //enables to interact with system processes, logs and performance counters

namespace ServerSide
{
    public partial class ServerSide : Form
    {
        private Socket _serverSocket; //The Server Side Socket
        private Socket _clientSocket; //The Client Side Socket
        private byte[] _buffer;

        public ServerSide()
        {
            //Boots the client within the server, will need to be adjusted for other systems
            string path = @"L:\Systems Programming RESIT\ClientSide\ClientSide\bin\Debug\ClientSide.exe";
            Process.Start(path);

            InitializeComponent();
            //Method responsible of creating, binding and listening for connections
            StartServer();
        }
        private void StartServer()
        {
            try
            {
                //creates a socket that can be used to communicate on a TCP/IP-based network
                _serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                //bind on any local interface with a specific port 
                _serverSocket.Bind(new IPEndPoint(IPAddress.Any, 8000));
                //Server Socket in a listening state with a fixed length of pending connections queue.
                _serverSocket.Listen(10);
                //begins an asynchronous operation to accept an incoming connection attempt
                _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
            }
            catch (Exception ex)
            {
                //error message will display if failed to establish a connection with the socket
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AcceptCallback(IAsyncResult AR) //this is called when a new connection request is received on the socket
        {
            try
            {
                //asynchronously accepts an incoming connection attempt
                _clientSocket = _serverSocket.EndAccept(AR);
                //gets or sets a value that specifies the size of the receive buffer of the Socket.
                _buffer = new byte[_clientSocket.ReceiveBufferSize];
                //begin to asynchronously receive data from a connected socket
                _clientSocket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), null);
                //begins an asynchronous operation to accept an incoming connection attempt
                _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);

                //message will appear on the serverside to indicate a connection has been successful
                AppendToTextBox("Client has connected"); 
            }

            catch (Exception ex)
            {
                //otherwise yet again will show a failure if failed to make a connection
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ReceiveCallback(IAsyncResult AR)
        {
            try
            {
                //ends a pending asynchronous read, completing the asynchronous method started in the BeginReceive method 
                int received = _clientSocket.EndReceive(AR);

                if (received == 0)
                {
                    return;
                }

                Array.Resize(ref _buffer, received);
                string text = Encoding.ASCII.GetString(_buffer); 

                if (text == "-exit") //if client enters text it will request to close the bulletin board
                {
                    _clientSocket.Close();
                    _serverSocket.Close(); //both the client and the server socket closes, resulting in the application to close
                    Application.Exit();
                }

                if (text == "-news") //client requests news topic
                {
                    
                }

                //
                Array.Resize(ref _buffer, _clientSocket.ReceiveBufferSize);
                AppendToTextBox("Client says:" + text);
                //implements the asynccallback delegate 
                _clientSocket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void AppendToTextBox(string text)
        {
            //a delegate that can execute any method in managed code that is declared void and takes no parameters
            MethodInvoker invoker = new MethodInvoker(delegate
            {
                textBox.Text += text + "\r\n";
            });
            this.Invoke(invoker);
        }

        private void ServerSide_Load(object sender, EventArgs e)
        {

        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
