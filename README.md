•	Network communication using TCP/IP Protocol suite

•	I created a client side bulletin board that allowed users to submit and view
—— from a repository held at a remote server (within local network).

•	Users (Client) are able to send messages to the server, 
which were to be added to the list of messages relating to a particular topic. 
This was done using a client to server architecture.

•	The server is capable of receiving and storing messages, 
relating to the particular topic and responds with a reply with the related topic message when requested from the user.

•	The application was written in C# and developed with windows sockets/UDP/TCP.
